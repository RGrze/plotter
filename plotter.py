import sys
import csv
from PyQt5 import QtWidgets, uic
import os.path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from itertools import cycle, zip_longest

class MyApp(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyApp,self).__init__()                                         #
        uic.loadUi('plotter.ui', self)
        self.hide_range_labels()
        self.pushButton_file_browser.clicked.connect(self.FileBrowser)
        self.pushButton_plot_1.clicked.connect(self.plot_x_vs_y)
        self.pushButton_plot_2.clicked.connect(self.plot_cap_vs_cycles)
        self.pushButton_selected_items_uparrow.clicked.connect(self.move_items_at_list)
        self.pushButton_selected_items_downarrow.clicked.connect(self.move_items_at_list)
        self.pushButton_selected_items_clear.clicked.connect(self.clear_list)
        self.cBox_select_range_cap.stateChanged.connect(self.show_range_elements)
        self.cBox_select_range_vol.stateChanged.connect(self.show_range_elements)
        self.cB_x_axis.currentIndexChanged.connect(self.update_button_text)
        self.cB_y_axis.currentIndexChanged.connect(self.update_button_text)
        self.full_directory={}
        self.unit_dictionary = {'Capacity': 'mAh',
                                'Voltage': 'V',
                                'Time': 's',
                                'time': 's',
                                'Current': 'A'}
    def update_button_text(self):
        self.pushButton_plot_1.setText('{} vs {}'.format(self.cB_y_axis.currentText(),
                                                         self.cB_x_axis.currentText()))
    def hide_range_labels(self):
        self.lab_range.hide()
        self.lab_unit.hide()
        self.label_5.hide()
        self.lE_rang_low.hide()
        self.lE_rang_high.hide()
    def show_range_elements(self):
        sender = self.sender()
        if sender.isChecked():
            if sender.objectName() == 'cBox_select_range_cap':
                self.lab_range.setText('Range Capacity')
                self.lab_unit.setText('(mAh)')
                self.cBox_select_range_vol.setEnabled(False)
            elif sender.objectName() == 'cBox_select_range_vol':
                self.lab_range.setText('Range Voltage')
                self.lab_unit.setText('(V)')
                self.cBox_select_range_cap.setEnabled(False)
            self.lab_range.show()
            self.lab_unit.show()
            self.label_5.show()
            self.lE_rang_low.show()
            self.lE_rang_high.show()
        else:
            self.hide_range_labels()
            self.lE_rang_low.clear()
            self.lE_rang_high.clear()
            self.cBox_select_range_cap.setEnabled(True)
            self.cBox_select_range_vol.setEnabled(True)
    def clear_list(self):
        self.listWidget_selected_items.clear()
        self.full_directory.clear()
    def FileBrowser(self):
        """open a filebrowser where you choose files to plot,
        populate files list with filenames, add records to dictionary which
         holds full directory to file"""
        try:
            filePaths = QtWidgets.QFileDialog.getOpenFileNames(None, 'Select files',
                                           "C:\\Users\\Measurement\\Documents\\Measurements",
                                           "CSV File (*.csv);;HDF File (*.h5)")[0]
            for file in filePaths:
                file = os.path.normpath(file)
                filename = os.path.basename(file)
                self.full_directory[filename] = file
                self.listWidget_selected_items.addItem(filename)
            if file[-3:] == 'csv':
                headers = self.get_headers(file)
                self.populate_axes_combobox(headers)
                self.sampleID = file.split(os.sep)[-3]
            else:
                headers = ['Voltage', 'Current', 'Time', 'Capacity']
                self.populate_axes_combobox(headers)
                self.sampleID = file.split(os.sep)[-4]
        except UnboundLocalError: #we pass the error when file dialog is closed by X button
            pass
    def populate_axes_combobox(self, headers):
        """clear combo boxes values from previous records, populate them with headers of new files
        select Voltage for y-axis and Capacity for x-axis as default"""
        self.cB_x_axis.clear()
        self.cB_y_axis.clear()
        for item in headers:
           self.cB_x_axis.addItem(item)
           self.cB_y_axis.addItem(item)
        self.cB_x_axis.setCurrentIndex(self.cB_x_axis.findText('Capacity'))
        self.cB_y_axis.setCurrentIndex(self.cB_x_axis.findText('Voltage'))
    def load_plot_colors(self):
        plot_colors=['b', 'g', 'r', 'c', 'm', 'y', 'k', 'aqua', 'crimson',
                     'darkgreen', 'indigo', 'navy', 'orange', 'purple', 'sienna',
                     'brown', 'darkgray', 'darkorange', 'darkslategrey']
        return plot_colors
    def move_items_at_list(self):
        sender=self.sender()
        lista=self.listWidget_selected_items
        currentRow = lista.currentRow()
        currentItem = lista.takeItem(currentRow)
        if 'uparrow' in sender.objectName():
            lista.insertItem(currentRow - 1, currentItem)
        else:
            lista.insertItem(currentRow + 1, currentItem)
        lista.setCurrentItem(currentItem)
    def get_headers(self, file):
        """ get column names from the first row of csv file """
        with open(file, 'r') as f:
            d_reader = csv.DictReader(f, delimiter='\t')
            return d_reader.fieldnames
    def items_to_load_rest(self):
        """if operation type is charge/discharge, function will iterate over list to find
        items with proper operation type in their filename
        if operation type is None - function will load all the files in a list"""
        operation_type = self.cB_select_type_scatter_plot.currentText()
        filename_list=[]
        #iterate over items' list:
        for row in range(self.listWidget_selected_items.count()):
            #get a text of selected item in list:
            filename = self.listWidget_selected_items.item(row).text()
            if 'None' not in operation_type:
                #if operation_type match operation type in filename, append list to plot:
                if operation_type == filename.split('_')[-2]:
                    filename_list.append(filename)
            else:
                filename_list.append(filename)
        filename_list.sort()
        return filename_list
    def items_to_load_all_together(self):
        """if all together is chosen, we create two lists - one for charge and one for discharge
           we sort the filenames with ascending order"""
        filename_list_charge=[]
        filename_list_discharge=[]
        for row in range(self.listWidget_selected_items.count()):
            filename = self.listWidget_selected_items.item(row).text()
            if 'charge' == filename.split('_')[-2]:
                filename_list_charge.append(filename)
            elif 'discharge' == filename.split('_')[-2]:
                filename_list_discharge.append(filename)
        filename_list_discharge.sort()
        filename_list_charge.sort()
        return (filename_list_charge, filename_list_discharge)
    def get_cycle_number(self, filename):
        """split the filename to get operation number.
        Cycle number: if operation number is even we divide it by 2,
        if operation number is odd, we add 1 and then divide by 2"""
        operation_number = int(filename.split('_')[-1].split('.')[0])
        if not operation_number %2:
            cycle_number = operation_number//2
        else:
            cycle_number = (operation_number+1)//2
        return cycle_number
    def load_files(self):
        files_to_load, operation_type = self.get_items_with_proper_operation_type()
        slownik_df = {}
        if operation_type == 'charge' or operation_type == 'discharge':
            for file in files_to_load:
                slownik_df[self.get_cycle_number(file)] = pd.read_csv(self.full_directory[file], sep='\t')
            return slownik_df, operation_type
        elif operation_type == 'all together':
            slownik_df_charge = {}
            slownik_df_discharge = {}
            for file in files_to_load[0]: #charge list
                slownik_df_charge[self.get_cycle_number(file)] = pd.read_csv(self.full_directory[file], sep='\t')
            for file in files_to_load[1]: #discharge list
                slownik_df_discharge[self.get_cycle_number(file)] = pd.read_csv(self.full_directory[file], sep='\t')
            return (slownik_df_charge, slownik_df_discharge), operation_type
        else:
            for file in files_to_load:
                slownik_df[file] = pd.read_csv(self.full_directory[file], sep='\t')
            return slownik_df, operation_type
    def get_capacity_from_last_line(self, path, n=1):
        """function reads the last line of log file.
        return: list with last string. We get the 1st position of the list (since we read only 1 line,
        split the columns which are separated by tab, and read last column which is Capacity in our
        log files"""
        fp = open(path)
        for i in range(n):
            line = fp.readline()
            if line == '':
                return []
        back = open(path)
        for each in fp:
            back.readline()
        result = []
        for line in back:
            result.append(line[:-1])
        return float(result[0].split('\t')[-2])
    def create_capacity_list_rest(self):
        """we append lists with capacity and corresponding number of cycle
        returns lists: capacity of files, number of cycles and the operation type"""
        files_to_load = self.items_to_load_rest()
        capacity_list = []
        num_of_cycle = []
        for file in files_to_load:
            capacity_list.append(self.get_capacity_from_last_line(self.full_directory[file]))
            num_of_cycle.append(self.get_cycle_number(file))
        num_of_cycle, capacity_list = zip(*sorted(zip(num_of_cycle, capacity_list)))
        return capacity_list, num_of_cycle
    def create_capacity_list_all_together(self):
        """if all together mode was chosen, we create lists of capacity and corresponding number
        of cycles for both charge and discharge operations separately"""
        # files to load is a tuple, where [0] are charge files and [1] are discharge files 
        files_to_load = self.items_to_load_all_together()
        capacity_charge = []
        num_of_cycle_charge = []
        capacity_discharge = []
        num_of_cycle_discharge= []
        for file in files_to_load[0]: #charge files
            capacity_charge.append(self.get_capacity_from_last_line(self.full_directory[file]))
            num_of_cycle_charge.append(self.get_cycle_number(file))
        for file in files_to_load[1]: #discharge files
            capacity_discharge.append(self.get_capacity_from_last_line(self.full_directory[file]))
            num_of_cycle_discharge.append(self.get_cycle_number(file))
        # sort the items by number of cycle:
        num_of_cycle_charge, capacity_charge = zip(*sorted(zip(num_of_cycle_charge, capacity_charge)))
        num_of_cycle_discharge, capacity_discharge = zip(*sorted(zip(num_of_cycle_discharge, capacity_discharge)))
        return (capacity_charge, capacity_discharge), (num_of_cycle_charge, num_of_cycle_discharge)
    def get_linear_coefficient_rest(self, capacity_list, num_of_cycle_list):
        #find max value in capacity list to start with:
        index = np.argmax(capacity_list)
        #calculate linear coefficient starting from maximum capacity and return it as list:
        coeff = np.polyfit(num_of_cycle_list[index:], capacity_list[index:], 1)
        #returns capacity loss in percent after 100 cycles:
        return ((coeff[0]*100)/capacity_list[index])*100
    def get_linear_coefficient_all_together(self, capacity_list, num_of_cycle_list):
        #get index(number of cycle) of highest capacity
        index_c = np.argmax(capacity_list[0])
        index_d = np.argmax(capacity_list[1])
        #calculate linear regression coefficients starting from max capacity:
        coeff_c = np.polyfit(num_of_cycle_list[0][index_c:], capacity_list[0][index_c:], 1)
        coeff_d = np.polyfit(num_of_cycle_list[1][index_d:], capacity_list[1][index_d:], 1)
        #calculate capacity loss after 100 cycles in %:
        capacity_loss_charge = ((coeff_c[0]*100)/capacity_list[0][index_c])*100
        capacity_loss_discharge = ((coeff_d[0]*100)/capacity_list[1][index_d])*100
        return (capacity_loss_charge, capacity_loss_discharge)
    def init_plot_window(self):
        plt.ion()
        self.fig=plt.figure()
        self.ax1=self.fig.add_subplot(111)
    def plot_cap_vs_cycles(self):
        self.init_plot_window()
        color = self.load_plot_colors()
        operation_type = self.cB_select_type_scatter_plot.currentText()
        if operation_type != 'all together':
            capacity, num_of_cycle = self.create_capacity_list_rest()
            capacity_loss = self.get_linear_coefficient_rest(capacity, num_of_cycle)
            self.ax1.set_title('{} capacity versus number of cycles for {}'.format(operation_type, self.sampleID))
            self.ax1.plot(num_of_cycle, capacity, '+', markersize='10', label=operation_type)
            self.ax1.text(0.05, 0.01, 'Capacity loss after 100 cycles: {:3f} %'.format(capacity_loss),
                          verticalalignment='bottom', horizontalalignment='left',
                          transform=self.ax1.transAxes,
                          color='black', fontsize=7)
        else:
            capacity, num_of_cycle = self.create_capacity_list_all_together()
            capacity_loss = self.get_linear_coefficient_all_together(capacity, num_of_cycle)
            self.ax1.set_title('Capacity versus number of cycles for {}'.format(self.sampleID))
            self.ax1.plot(num_of_cycle[0], capacity[0], '+',
                          color='b', markersize='10', label="charge")
            self.ax1.plot(num_of_cycle[1], capacity[1], '+',
                          color='k', markersize ='10', label="discharge")
            self.ax1.text(0.05, 0.01, ("Capacity loss after 100 cycles:\n"
                                       "ch: {:3f} %\n"
                                       "dch: {:3f} %").format(capacity_loss[0], capacity_loss[1]),
                          verticalalignment='bottom', horizontalalignment='left',
                          transform=self.ax1.transAxes, color='black', fontsize=7)
        self.ax1.set_xlabel('Num of cycle')
        self.ax1.set_ylabel('Capacity, mAh')
        self.ax1.legend().remove()
        self.fig.show()
    def plot_x_vs_y(self):
        """main function to plot"""
        #Initiate new plot window, set color cycle, get operation type and axes to plot:
        self.init_plot_window()
        color = cycle(self.load_plot_colors())
        operation_type = self.cB_select_type_scatter_plot.currentText()
        x_ax, y_ax = self.cB_x_axis.currentText(), self.cB_y_axis.currentText()
        if operation_type != 'all together':
            #get files to plot:
            files = self.items_to_load_rest()
            for file in files:
                if 'None' not in operation_type:
                    n = self.get_cycle_number(file)
                    self.load_and_plot(file, x_ax, y_ax, '-', next(color), n)
                else:
                    self.load_and_plot_nonstandard(file, x_ax, y_ax, '-', next(color))
        else:
            files = self.items_to_load_all_together()
            #if there's the same amount of charge and discharge files, we just iterate over them:
            if len(files[0]) == len(files[1]):
                for i, j in zip(files[0], files[1]):
                    n1 = self.get_cycle_number(i)
                    n2 = self.get_cycle_number(j)
                    cl = next(color)
                    self.load_and_plot(i, x_ax, y_ax, '-', cl, n1)
                    self.load_and_plot(j, x_ax, y_ax, '--', cl, n2)
            # print(files)
            else:
            #needs more testing/improvement/clearance:
                for i, j in zip_longest(files[0],files[1]):
                    if i != None:
                        n1 = self.get_cycle_number(i)
                    else:
                        n1 = None
                    if j != None:
                        n2 = self.get_cycle_number(j)
                    else:
                        n2 = None
                    if n1 == n2:
                        cl = next(color)
                        self.load_and_plot(i, x_ax, y_ax, '-', cl, n1)
                        self.load_and_plot(j, x_ax, y_ax, '--', cl, n2)
                    else:
                        if i != None:
                            self.load_and_plot(i, x_ax, y_ax, '-', next(color), n1)
                        if j != None:
                            self.load_and_plot(j, x_ax, y_ax, '--', next(color), n2)
        self.ax1.set_xlabel('{}, {}'.format(x_ax, self.unit_dictionary[x_ax]))
        self.ax1.set_ylabel('{}, {}'.format(y_ax, self.unit_dictionary[y_ax]))
        self.ax1.set_title('{} versus {} for {}'.format(y_ax, x_ax, self.sampleID))
        self.ax1.legend().remove()
        self.fig.show()

    def load_and_plot(self, file, x_ax, y_ax, ls, color, cycle):
        file_split = file.split('_')
        sample = file_split[-3]
        operation = file_split[-2]
        if file[-2:] == 'h5':
            d = pd.read_hdf(self.full_directory[file])
        else:
            d = pd.read_csv(self.full_directory[file], sep='\t',
                            usecols=[x_ax, y_ax], dtype='float32')
        if self.cBox_select_range_cap.isChecked():
            bottom = float(self.lE_rang_low.text())
            top = float(self.lE_rang_high.text())
            d = d[d['Capacity'].between(bottom, top)]
        elif self.cBox_select_range_vol.isChecked():
            bottom = float(self.lE_rang_low.text())
            top = float(self.lE_rang_high.text())
            d = d[d['Voltage'].between(bottom, top)]
        d.plot(x_ax, y_ax, ax=self.ax1, color=color, style=ls,
               label='{}, {} {}'.format(sample, operation, cycle))

    def load_and_plot_nonstandard(self, file, x_ax, y_ax, ls, color):
        d = pd.read_csv(self.full_directory[file], sep='\t',
                           usecols=[x_ax, y_ax], dtype='float32')
        d.plot(x_ax, y_ax, ax=self.ax1, color=color, style=ls)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
